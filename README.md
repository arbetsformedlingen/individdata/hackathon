# Hackaton

![hackaton system description](https://gitlab.com/arbetsformedlingen/individdata/hackathon/-/raw/main/images/hackaton20240313.png)
## Dagens Plan:
### 1: Implementera en Endpoint
Implementera en endpoint som returnerar data enligt specifikationen nedan.
<br>
```json
{
    "$id": "https://gitlab.com/arbetsformedlingen/individdata/hackathon/-/raw/main/kompetensprofilen.schema.json",
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "kompetensprofilen",
    "type": "object",
    "properties": {}
}
```
### 2: Konsumera denna endpoint